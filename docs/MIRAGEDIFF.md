# Differences between Moment and Mirage

## Maintenance

Due to access issues, it is unlikely
<a href="https://github.com/mirukana/mirage">Mirage</a>
will continue being maintained. You should check the
<a href="https://github.com/mirukana/mirage/commits/master">
date of the last commit</a> to make sure.

## UI

Moment adds a new default theme. If you would rather have the default
Mirage theme instead, add this to `~/.config/moment/settings.py`:
``` python
class General:
    theme: str = "Midnight.qpl"
```

## Features

There are currently no differences in features,
although Moment does have some bug fixes that Mirage does not.

## Configuration

Moment has a different configuration directory
(you will get an option to migrate your Mirage
config automatically on first startup).

Moment has different default keybindings:
- <kbd>Ctrl + Q</kbd> is `Quit`
- `Reply` is <kbd>Ctrl + R</kbd> instead of <kbd>Ctrl + Q</kbd>
- `Remove` is <kbd>Ctrl + Shift + R</kbd> instead of <kbd>Ctrl + R</kbd>
- <kbd>Ctrl + K</kbd> is `Focus filter`
- `Focus previous message` is <kbd>Ctrl + I</kbd> instead of  <kbd>Ctrl + K</kbd>

If you wish to have `Focus previous message` and `Focus next message`
adjacent on <kbd>Ctrl + U</kbd> and <kbd>Ctrl + I</kbd>,
we recommend swapping <kbd>Ctrl + U</kbd> and <kbd>Ctrl + J</kbd>
by adding this to `~/.config/moment/settings.py`:
``` python
class Keys:

    class Rooms:
        latest_unread = ["Ctrl+J"]

    class Messages:
        next = ["Ctrl+Down", "Ctrl+U"]
```

## Reverting to default Mirage bindings

If you wish to revert to default Mirage bindings,
add this to `~/.config/moment/settings.py`:
``` python
class Keys:

    quit = []

    class Rooms:
        focus_filter = ["Alt+F"]
        latest_unread = ["Ctrl+J"]

    class Messages:
        reply = ["Ctrl+Q"]
        remove = ["Ctrl+R"]
        previous = ["Ctrl+Up", "Ctrl+K"]
```
